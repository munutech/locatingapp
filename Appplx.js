/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  NativeModules,NativeEventEmitter,Button
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
// import BleManager from 'react-native-ble-plx';
import BleManager from 'react-native-ble-manager';
const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);
// import { BleManager } from 'react-native-ble-plx';


export default class App extends Component{

  constructor(props){
    super(props)
    // this.manager = new BleManager();
    this.state={
      data:[1,2]
    }
  }

  componentDidMount(){
   this.start()
      //检查蓝牙打开状态，初始化蓝牙后检查当前蓝牙有没有打开
    BleManager.checkState()
    //蓝牙状态改变监听
    bleManagerEmitter.addListener('BleManagerDidUpdateState', (args) => {
      // console.log('BleManagerDidUpdateStatea:', args);
      if(args.state == 'on' ){  //蓝牙已打开

      }
    });

    this.scan()
    const that = this
    //搜索到一个新设备监听
    bleManagerEmitter.addListener('BleManagerDiscoverPeripheral', (data) => {
      // console.log('BleManagerDiscoverPeripheral:', data);
      let id;  //蓝牙连接id
      let macAddress;  //蓝牙Mac地址            
      if(Platform.OS == 'android'){
          macAddress = data.id;
          id = macAddress;
      }else{  
          //ios连接时不需要用到Mac地址，但跨平台识别是否是同一设备时需要Mac地址
            //如果广播携带有Mac地址，ios可通过广播0x18获取蓝牙Mac地址，
          macAddress = that.getMacAddressFromIOS(data);
          id = data.id;
        }            
    });

  }
  componentWillMount() {
    // // 查看蓝牙状态
    // const subscription = this.manager.onStateChange((state) => {
    //     if (state === 'PoweredOn') {
    //         this.scanAndConnect();
    //         subscription.remove();
    //     }else{
    //       alert('请打开蓝牙')
    //     }
    // }, true);

        //蓝牙设备已连接监听
    bleManagerEmitter.addListener('BleManagerConnectPeripheral', (args) => {
      // log('BleManagerConnectPeripheral:', args);
    });

    //蓝牙设备已断开连接监听
    bleManagerEmitter.addListener('BleManagerDisconnectPeripheral', (args) => {
      // console.log('BleManagerDisconnectPeripheral:', args);
    });
}

 
  // 开始监听
  start = () => {
    BleManager.start({showAlert: false})
    .then( ()=>{
        // console.log('Init the module success.');                
    }).catch(error=>{
        // console.log('Init the module fail.');
    });

  }

  // 扫描可用设备
  scan = () => {
    BleManager.scan(['FE78'], 5, true)
    .then(() => {
        // console.log('Scan started');
    });

  }

  stopScan = () => {
    //停止扫描
    BleManager.stopScan()
    .then(() => {
        // console.log('Scan stopped');
    })
    .catch((err)=>{
        // console.log('Scan stopped fail',err);
    });

  }

  /** ios系统从蓝牙广播信息中获取蓝牙MAC地址 */
  getMacAddressFromIOS = (data) =>{
    let macAddressInAdvertising = data.advertising.kCBAdvDataManufacturerMacAddress;
    //为undefined代表此蓝牙广播信息里不包括Mac地址
    if(!macAddressInAdvertising){  
          return;
      }
    macAddressInAdvertising = macAddressInAdvertising.replace("<","").replace(">","").replace(" ","");
    if(macAddressInAdvertising != undefined && macAddressInAdvertising != null && macAddressInAdvertising != '') {
    macAddressInAdvertising = this.swapEndianWithColon(macAddressInAdvertising);
      }
      console.log(macAddressInAdvertising)

      return macAddressInAdvertising;
  }

  /**
  * ios从广播中获取的mac地址进行大小端格式互换，并加上冒号:
  * @param str         010000CAEA80
  * @returns string    80:EA:CA:00:00:01
  */
  swapEndianWithColon = (str) => {
    let format = '';
    let len = str.length;
    for(let j = 2; j <= len; j = j + 2){
      format += str.substring(len-j, len-(j-2));
      if(j != len) {
        format += ":";
      }
    }
      return format.toUpperCase();
  }
 

  render() { 
    // console.log(this.state.data)
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle} onPress={() => this.scan()}>扫描可ss用设备</Text>
            </View>
            {/* {
              this.state.data.map((item,index)=>{
                return(
                  <View>
                    <Text>111</Text>
                  </View>
                )
              })
            } */}


          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  )};
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

