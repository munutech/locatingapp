/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import Main from './Main';
import Appplx from './Appplx';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Main);
