/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  DeviceEventEmitter
} from 'react-native';
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-crop-picker';
import { WhiteSpace, InputItem, Picker, Provider, Modal } from '@ant-design/react-native';
import * as bkjs from './BackendJS/object.js'
import sq from './utils/SQLiteStorage';

export default class Addsteptwo extends Component{

  constructor(props){
    super(props)
    this.state={
      id:'',
      name:'',
      trackModeValue:1,
      distanceValue:1,
      timeValue:1,
      signalValue:1,
      trackMode:[
        {
          label: '登山',
          value: 1,
        },
        {
          label: '游泳',
          value: 2,
        },
      ],
      // distance:[
      //   {
      //     label: '500',
      //     value: 1,
      //   },
      // ],
      distance:0,
      time:[
        {
          label: '5min',
          value: 1,
        },
      ],
      signal:[
        {
          label: '低',
          value: 1,
        },
        {
          label: '中',
          value: 2,
        },
        {
          label: '高',
          value: 3,
        },
      ],
    }
  }

  componentDidMount(){
    const sql = 'CREATE TABLE IF NOT EXISTS DEVICE(' +
    'id INTEGER PRIMARY KEY  AUTOINCREMENT,' +
    'bt_id integer,' +
    'name varchar,'+
    'sn varchar,' +
    'mode INTEGER,' +
    'distance_threshold INTEGER,' +
    'polling_frequency INTEGER,' +
    'latitude REAL,' +
    'longitude REAL,' +
    'signal_threshold INTEGER)'
    sq.createTable(sql)

    if(this.props.id){
      sq.getMsgInfoFromTable('DEVICE', 'bt_id', this.props.id, (status, datas) =>{
        if(status){
          this.setState({
            name:datas.name,
            trackModeValue:datas.mode,
            signalValue:datas.signal_threshold,
            distance:datas.distance_threshold.toString(),
            timeValue:datas.polling_frequency
          })
        }
      })

    }

    DeviceEventEmitter.emit('getValue',this.state)

  }

  UNSAFE_componentWillMount(){
    DeviceEventEmitter.emit('getValue',this.state)

  }

  trackModeClick=(v)=>{
    this.setState({
      trackModeValue:v
    })
  }
  distanceClick=(v)=>{
    this.setState({
      distanceValue:v
    })
  }
  timeClick=(v)=>{
    this.setState({
      timeValue:v
    })
  }
  signalClick=(v)=>{
    this.setState({
      signalValue:v
    })
  }

  onButtonClick2 = () => {
    Modal.operation([
      { text: '打開相機', onPress: () => this.imgClick() },
      { text: '打開相冊', onPress: () => this.imgClick2() },
      { text: '取消', onPress: () => console.log('置顶聊天被点击了') },
    ]);
  };
  imgClick=()=>{
    ImagePicker.openCamera({
      width:300,
      height:400,
      cropping:true
    }).then(image => {
        let source = {uri: image.path};
    
        this.setState({
            avatarSource: source  // 将图片存于本地
        });
    });
    


  }

  imgClick2 = () => {
    ImagePicker.openPicker({
      width:300,
      height:400,
      cropping: true
    }).then(image => {
        let source = {uri: image.path};
    
        // this._fetchImage(image);
    
        this.setState({
          avatarSource: source
        });
    });
  }

  onSubmit = () =>{
    const obj = {
      "sn" :  this.props.qrcode, //"22222222", // serial number
      "bt_id" : this.props.qrcode, //"bt123456", // bluetooth id
      "name" :  this.state.name, // alias name
      "mode" :  this.state.trackModeValue, // 登山, …..etc
      "distance_threshold" : this.state.distance, // float
      "polling_frequency" : this.state.timeValue, //sec
      "signal_threshold" :  this.state.signalValue, // float
      "latitude" :37.8025229,
      "longitude" : -122.4351439,
    }
    
    if(this.props.id){
      sq.updateDataToTable('DEVICE','bt_id',this.props.id,obj)
    }else{
      sq.insertDataToTable('DEVICE', obj)

    }

    // Actions.app()
  }

  delete = () => {
    bkjs.ondelete()
  }

  render() {

  return (
    <Provider>
      <View style={styles.container}>
        <WhiteSpace size="xl" />
        <Text>請設定和定義感應器資訊</Text>
        <WhiteSpace size="xl" />
        <View style={styles.img}>
{/* <Image source={require('file:///storage/emulated/0/Android/data/com.awesomeproject/files/Pictures/ee91c693-fec0-4dc1-8d49-5c7f38ca6f61.jpg')} /> */}
        </View>
        <WhiteSpace size="sm" />
        <Text onPress={()=>this.onButtonClick2()}>上傳大頭貼</Text>
        <WhiteSpace size="lg" />
        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-end'}}>
          <Icon name="map-marker" style={{marginBottom:10}} size={25} color="#0079ff" />
          <View style={{width:'100%'}}>
            <Text style={styles.lable}>追蹤目標</Text>
            <InputItem
              clear
                value={this.state.name}
                style={styles.input}
                placeholder="追蹤目標"
                onChange={value => {
                  this.setState({
                    name: value,
                  });
                }}
              >
                {/* <Icon name="map-marker" style={{marginRight:-30}} size={25} color="#0079ff" /> */}
              
            </InputItem>
          </View>
        </View>
        <WhiteSpace size="sm" />
        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-end'}}>
          <Icon name="map-marker" style={{marginBottom:15}} size={25} color="#0079ff" />
          <View style={{width:'100%'}}>
            <Text style={styles.lable}>追蹤物件模式</Text>
            <Picker
              title=""
              data={this.state.trackMode}
              cols={1}
              value={this.state.trackModeValue}
              onOk={this.trackModeClick}
            >
              <Text style={styles.select}>
                {this.state.trackMode[parseInt(this.state.trackModeValue)-1].label}
              </Text>
            </Picker>

          </View>
        </View>
        <WhiteSpace size="sm" />
        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-end'}}>
          <Icon name="map-marker" style={{marginBottom:15}} size={25} color="#0079ff" />
          <View style={{width:'100%'}}>
            <Text style={styles.lable}>安全距離(最大偵測範圍為15公里超出範圍顯示警告)</Text>
            <InputItem
              clear
                value={this.state.distance}
                style={styles.input}
                placeholder="安全距離（公里）"
                onChange={value => {
                  this.setState({
                    distance: value,
                  });
                }}
              />
            {/* <Picker
              title=""
              data={this.state.distance}
              cols={1}
              value={this.state.distanceValue}
              onOk={this.distanceClick}
            >
              <Text style={styles.select}>
                  500公尺
              </Text>
            </Picker> */}

          </View>
        </View>
        <WhiteSpace size="sm" />
        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-end'}}>
          <Icon name="clock-o" style={{marginBottom:15}} size={25} color="#0079ff" />
          <View style={{width:'100%'}}>
            <Text style={styles.lable}>連續頻率時間</Text>
            <Picker
              title=""
              data={this.state.time}
              cols={1}
              value={this.state.timeValue}
              onOk={this.timeClick}
            >
              <Text style={styles.select}>
                  每5分鐘
              </Text>
            </Picker>

          </View>
        </View>
        <WhiteSpace size="sm" />
        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-end'}}>
          <Icon name="clock-o" style={{marginBottom:15}} size={25} color="#0079ff" />
          <View style={{width:'100%'}}>
            <Text style={styles.lable}>訊號狀態警告基準</Text>
            <Picker
              title=""
              data={this.state.signal}
              cols={1}
              value={this.state.signalValue}
              onOk={()=>this.signalClick()}
            >
              <Text style={styles.selectSm}>
                  中
              </Text>
            </Picker>

          </View>
        </View>  
        <WhiteSpace size="sm" />
        <Text onPress={()=>this.onSubmit()}>提交</Text>
        <Text onPress={()=>this.delete()}>删除</Text>
      </View>
    </Provider>
    
  )};
};

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems:'center',
    padding:20
    // backgroundColor:'#535353'
  },
  img:{
    width:80,
    height:80,
    backgroundColor:'#ccc',
    borderRadius:40
  },
  InputItem: {
    flex:1,
    flexDirection:'row',
    width:'100%',
    padding:20,
  },
  input:{
    width:'100%',
    fontSize:14

  },
  lable:{
    width:'100%',
    color:'#666',
    marginLeft:10
  },
  select:{
    marginLeft:15,
    borderWidth:1,
    borderRadius:6,
    height:36,
    lineHeight:34,
    paddingHorizontal:10,
    borderColor:'#cbc9c9',
    fontSize:14,
    marginVertical:10
  },
  selectSm:{
    width:100,
    marginLeft:15,
    borderWidth:1,
    borderRadius:6,
    height:36,
    lineHeight:34,
    paddingHorizontal:10,
    borderColor:'#cbc9c9',
    fontSize:14,
    marginVertical:10    
  }
});

