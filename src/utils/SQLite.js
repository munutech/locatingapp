//SQLite.js
import React, { Component } from 'react';
import {
    ToastAndroid,
} from 'react-native';
import SQLiteStorage from 'react-native-sqlite-storage';

SQLiteStorage.DEBUG(true);
var database_name = "test.db";//数据库文件
var database_version = "1.0";//版本号
var database_displayname = "MySQLite";
var database_size = -1;
var db;

export default class SQLite extends Component {

    UNSAFE_componentWillMount(){
        if(db){
            this._successCB('close');
            db.close();
        }else {
            console.log("SQLiteStorage not open");
        }
    }
    open(){
        db = SQLiteStorage.openDatabase(
            database_name,
            database_version,
            database_displayname,
            database_size,
            ()=>{
                this._successCB('open');
            },
            (err)=>{
                this._errorCB('open',err);
            });
        return db;
    }

    createTable(){

        if (!db) {

            this.open();
        }
        //创建用户表
        db.transaction((tx)=> {
            tx.executeSql('CREATE TABLE IF NOT EXISTS USER(' +
                'id INTEGER PRIMARY KEY  AUTOINCREMENT,' +
                'name varchar,'+
                'age VARCHAR,' +
                'sex VARCHAR,' +
                'phone VARCHAR,' +
                'email VARCHAR,' +
                'address VARCHAR)'
                , [], ()=> {
                    this._successCB('executeSql');
                }, (err)=> {
                    this._errorCB('executeSql', err);
                });
        }, (err)=> {//所有的 transaction都应该有错误的回调方法，在方法里面打印异常信息，不然你可能不会知道哪里出错了。
            this._errorCB('transaction', err);
        }, ()=> {
            this._successCB('transaction');
        })

         //创建device表
         db.transaction((tx)=> {
            tx.executeSql('CREATE TABLE IF NOT EXISTS DEVICE(' +
                'id INTEGER PRIMARY KEY  AUTOINCREMENT,' +
                'bt_id integer,' +
                'name varchar,'+
                'sn varchar,' +
                'mode INTEGER,' +
                'distance_threshold INTEGER,' +
                'polling_frequency INTEGER,' +
                'latitude REAL,' +
                'longitude REAL,' +
                'signal_threshold INTEGER)'
                , [], ()=> {
                    this._successCB('executeSql');
                }, (err)=> {
                    this._errorCB('error', err);
                });
        }, (err)=> {//所有的 transaction都应该有错误的回调方法，在方法里面打印异常信息，不然你可能不会知道哪里出错了。
            this._errorCB('transaction', err);
        }, ()=> {
            this._successCB('transaction');
        })
    }

    //删除制定一项
    deleteDataFromTable(value) {
        if (!db) {
            this.open();
        }
        db.transaction((tx)=>{
            tx.executeSql(`DELETE FROM DEVICE WHERE bt_id = ${value}`,[],()=>{

            });
        });
    }

    deleteData(){
        if (!db) {
            this.open();
        }
        db.transaction((tx)=>{
            tx.executeSql('delete from device',[],()=>{

            });
        });
    }
    dropTable(){
        db.transaction((tx)=>{
            tx.executeSql('drop table device',[],()=>{

            });
        },(err)=>{
            this._errorCB('transaction', err);
        },()=>{
            this._successCB('transaction');
        });
    }
    insertUserData(userData){

        let len = userData.length;
        if (!db) {
            this.open();
        }
        // this.createTable();
        // this.deleteData();
        db.transaction((tx)=>{
            for(let i=0; i<len; i++){
                var user = userData[i];
                let name= user.name;
                let age = user.age;
                let sex = user.sex;
                let phone = user.phone;
                let email = user.email;
                let address = user.address;
                let sql = "INSERT INTO user(name,age,sex,phone,email,address)"+
                    "values(?,?,?,?,?,?)";
                tx.executeSql(sql,[name,age,sex,phone,email,address],()=>{

                    },(err)=>{
                        console.log(err);
                    }
                );
            }
        },(error)=>{
            this._errorCB('err', error);
        },()=>{
            this._successCB('transaction insert data');
        });
    }
    // device 
    insertDeviceData(deviceData){
        console.log(deviceData)
        let len = deviceData.length;
        if (!db) {
            this.open();
        }
        // this.createTable();
        // this.deleteData();
        db.transaction((tx)=>{
            for(let i=0; i<len; i++){
                var device = deviceData[i];
                console.log(device)
                let sn= device.sn;
                let bt_id = device.bt_id;
                let name = device.name;
                let mode = device.mode;
                let distance_threshold = device.distance_threshold;
                let polling_frequency = device.polling_frequency;
                let signal_threshold = device.signal_threshold;
                let latitude = device.latitude;
                let longitude = device.longitude;
                let sql = "INSERT INTO device(sn,bt_id,name,mode,distance_threshold,polling_frequency,signal_threshold,latitude,longitude)"+
                // let sql = "INSERT INTO device(bt_id)"+
                    "values(?,?,?,?,?,?,?,?,?)";
                tx.executeSql(sql,[sn,bt_id,name,mode,distance_threshold,polling_frequency,signal_threshold,latitude,longitude],()=>{
                    // tx.executeSql(sql,[bt_id,],()=>{

                    },(err)=>{
                        console.log(err);
                    }
                );
            }
        },(error)=>{
            this._errorCB('err', error);
        },()=>{
            this._successCB('transaction insert data');
        });
    }
    close(){
        if(db){
            this._successCB('close');
            db.close();
        }else {
            console.log("SQLiteStorage not open");
        }
        db = null;
    }
    _successCB(name){
        console.log("SQLiteStorage "+name+" success");
    }
    _errorCB(name, err){
        console.log("SQLiteStorage "+name);
        console.log(err);
    }


    //查询device
    searchDevice(){
        if (!db) {
            this.open();
        }
        db.transaction((tx)=>{
            const devicedata = []
            tx.executeSql("select * from device", [],(tx,results)=>{

                var len = results.rows.length;
                const obj = []
                for(let i=0; i<len; i++){
                    var u = results.rows.item(i);
                    const device = {
                      "sn" :  u.sn, //"22222222", // serial number
                      "bt_id" : u.bt_id, //"bt123456", // bluetooth id
                      "name" :  u.name, // alias name
                      "mode" :  u.mode, // 登山, …..etc
                      "distance_threshold" : u.distance_threshold, // float
                      "polling_frequency" : u.polling_frequency, //sec
                      "signal_threshold" :  u.signal_threshold, // float
                      "latitude" : u.latitude,
                      "longitude" : u.longitude
                    }
                    obj.push(device)

                }
                devicedata = obj
                 
            });
            return devicedata
        },(error)=>{
            console.log(error);
        });
    }


    render(){
        return null;
    }
}
