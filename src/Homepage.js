/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component } from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  NativeModules,
  Platform,
  Alert,
  TouchableOpacity,
  DeviceEventEmitter
} from 'react-native';
import MapView , {PROVIDER_GOOGLE, Marker ,Callout, Circle , Polygon} from 'react-native-maps';
import { Provider,Tabs, Drawer, List, Modal } from '@ant-design/react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import Echarts from 'native-echarts';

import { Actions } from 'react-native-router-flux'
import Geolocation from '@react-native-community/geolocation';
import {request, PERMISSIONS} from 'react-native-permissions';
// import BleManager from 'react-native-ble-manager';
// import { View } from 'react-native';
import { isIphoneX } from './utils/isIphonex';
const BleManagerModule = NativeModules.BleManager;
// const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);
import { GiftedChat, Bubble } from 'react-native-gifted-chat'
// const CreatCat = NativeModules.CreatCat;
import * as bkjs from './BackendJS/object'
import sq from './utils/SQLiteStorage';

export default class Homepage extends Component{

  constructor(props){
    super(props)
    this.state={
      id:'',
      isDelete:false,// 设备删除
      devicedata:[],//设备信息
      show:'close',//抽屉开启关闭
      radius:1000,//circle 范围
      markers:[],// map marker data
      circlelatitude:0,
      circlelongitude:0,
      aa: [
        {
          id:0,
          name: 'jeckey',
          coordinates: {
            latitude: 37.8025259,
            longitude: -122.4351431,
          },
          radius:500
        },
        {
          id:1,
          name: 'well',
          coordinates: {
            latitude: 37.7896386,
            longitude: -122.421646,
          },
          radius:2000 
        },
        {
          id:2,
          name: 'jeckey',
          coordinates: {
            latitude: 37.8025229,
            longitude: -122.4351439,
          },
          radius:500
        },
      ],
      messages:[{
        _id: 2,
        text: '微信小程序开发的基本流程',
        createdAt: new Date('2018-10-25T15:41:00+08:00'),
        received:true,
        user: {
          _id: 1,
          name: 'jackson影琪',
          avatar: 'https://pic.cnblogs.com/avatar/1040068/20181013100635.png',
        },
        //image: 'https://img2018.cnblogs.com/blog/1040068/201810/1040068-20181024162047704-1159291775.png',
      },]
    }
  }
  //message send 
  onSend = (messages) => {
    this.setState(previousState=>({
      messages: GiftedChat.append(previousState.messages, messages),
    }))
  }


  onOpenChange = isOpen => {
    /* tslint:disable: no-console */
    console.log('是否打开了 Drawer', isOpen.toString());
  };
  aaa(){
    // alert("aa")
    // cat.cratCat('机器猫', '男', 2);
    // cat.addEvent("血雨声","zaijia")
    CreatCat.whoName("xieyusheng",12,"官渡")
  }
  componentDidMount(){
    // this.aaa()
    // this.test()
    this.requestLocationPermission()
    sq.initDB(1)

    sq.selectDataFromTable('DEVICE',(status, datas) =>{
      if(status){
        this.setState({
          devicedata:datas
        })
      }
    })
    
  //  this.getdata()

  }
  componentWillMount() {

  }

  test= () => {
    NativeModules.MyNativeModule.rnCallNative('rn调用native成功！')
  }
  show =(radius)=> {
    this.setState({
      show:'open',
      radius:radius
    })
    console.log()
    
  }
  close = () => {
    this.setState({
      show:'close'
    })
  }

  requestLocationPermission = async () => {
    if(Platform.OS === 'ios'){
      const response = await request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE)
      // console.log('ios'+ JSON.stringify(response))
      if(response === 'granted'){
        this.locateCurrentPosition()
      }
    }else{
      const response = await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
      // console.log('android'+ response)
      if(response === 'granted'){
        this.locateCurrentPosition()
      }
    }
  }

  locateCurrentPosition = () => {
    Geolocation.getCurrentPosition(
      position => {
        let region = {
          latitude:position.coords.latitude,
          longitude:position.coords.longitude,
          latitudeDelta: 0.09,
          longitudeDelta: 0.035,          
        }
        // let region = {
        //   latitude: 37.78825,
        //   longitude: -122.4324,
        //   latitudeDelta: 0.09,
        //   longitudeDelta: 0.035,
        // }
        this.setState({
          initialPosition:region,
          circlelatitude:position.coords.latitude,
          circlelongitude:position.coords.longitude
        })

      },
      error => Alert.alert(error.message),
      {enableHighAccuracy:true, timeout:20000,maximumAge:10000}
    )
  }

  onItemChange = (item,index) => {

    let location = this.state.devicedata[index]
    this._map.animateToRegion({
          latitude:location.latitude,
          longitude:location.longitude,
          latitudeDelta: 0.09,
          longitudeDelta: 0.035,        
    })
    this.drawer.closeDrawer()
    this.state.markers[index].showCallout()
    console.log(item)
    this.setState({
      show:'open',
      radius:item.distance_threshold,
      id:item.bt_id
    })
  }

  editDelete = () => {
    this.setState({
      isDelete:!this.state.isDelete
    })
  }

  onDelete = (item,index) =>{
    Modal.alert('Title', 'alert content', [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel') ,
        style: 'cancel',
      },
      { text: 'OK', onPress: () => this.deviceDelete(item.bt_id)},
    ]);
    console.log(index)
  }

  deviceDelete = (bt_id) => {
    sq.deleteDataFromTable('DEVICE', 'bt_id', bt_id,)
    sq.selectDataFromTable('DEVICE',(status, datas) =>{
      if(status){
        this.setState({
          devicedata:datas
        })
      }
    })
  }


  render() {
    const tabs = [
      { title: '傳送訊息' },
      { title: '歷史紀錄' },
    ];
    const tabstwo = [
      { title: '1天' },
      { title: '7天' },
      { title: '1個月' },
      { title: '6個月' },
      { title: '一年' },
      { title: '最久' },
    ];
    const option = {
      // title: {
      //     text: '安全距離500m以內'
      // },
      // tooltip: {},
      // legend: {
          // data:['销量']
      // },
      grid:{
        top:10,
        bottom:20
      },
      xAxis: {
        type:'category',
        data: ["00:00","06:00","12:00","18:00","24:00"]
      },
      yAxis: {
        type:'value',
      },
      series: [{
          // name: '销量',
          type: 'bar',
          data: [80, 532, 399, 234, 312]
      }]
    };
    const option2 = {
      grid:{
        top:10,
        bottom:20
      },
      xAxis: {
          type:'category',
          data: ["00:00","06:00","12:00","18:00","24:00"]
      },
      yAxis: {
        type:'value'
      },
      series: [{
          type: 'line',
          data: [5, 20, 36, 10, 10, 20]
      }]
    }
    const itemArr = this.state.devicedata.map((item, index) => {
          return (
            <List.Item
              key={index}
              thumb="https://zos.alipayobjects.com/rmsportal/eOZidTabPoEbPeU.png"
              multipleLine
              onPress={()=>this.state.isDelete?this.onDelete(item,index) : this.onItemChange(item,index)}
            >
              <View
                style={{
                  height:50,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}
              >
                <View>
                  <Text>{item.name} - {index}</Text>
                  <Text style={{marginTop:5}}>
                    <Icon name="clock-o" size={15} color="#000" />  2020/06/12 12:00
                  </Text>
                </View>
                {
                  this.state.isDelete?
                  <View>
                    <Text>
                      <Icon name="trash-o" size={20} color="#3572b0" />
                    </Text>
                  </View>:
                  <View
                    style={{
                      alignItems: 'flex-end',
                    }}
                  >
                    <Text>
                      <Icon name="signal" size={15} color="#3572b0" />
                    </Text>

                    <Text style={{marginTop:5}}>
                      距离｜{item.distance_threshold}
                    </Text>
                  </View>

                }



              </View>
            </List.Item>
          );

      });
      const sidebar = (
        <ScrollView style={{marginTop:isIphoneX()?44:0}}>
          {/* <Text style={styles.sideclose} onPress={() => this.drawer.closeDrawer()}>
            <Icon style={{textAlign:'center',lineHeight:26}} name="bars" size={15} color="#fff" />
          </Text> */}
          <List>{itemArr}</List>
          <View 
          style={{
            marginTop:15,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent:'space-around'
          }}>
            <View          
              style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
              <Icon name="plus-square-o" size={30} color="#000" />
              <Text style={{marginLeft:10}} onPress={()=>Actions.addstepone()}>新增追蹤物件</Text>
            </View>
          <Text style={{marginLeft:10}} onPress={()=>this.editDelete()}>{this.state.isDelete?'取消':'编辑'}</Text>
          </View>
        </ScrollView>
      );
  return (
    <Provider>
    <Drawer
    sidebar={sidebar}
    position="left"
    open={false}
    drawerRef={el => (this.drawer = el)}
    onOpenChange={this.onOpenChange}
    drawerBackgroundColor="#fff"
    > 
      <View style={styles.container}>
          <Text style={styles.sideshow} onPress={() => this.drawer && this.drawer.openDrawer()}>
            <Icon style={{textAlign:'center',lineHeight:26}} name="bars" size={15} color="#fff" />
          </Text>
            <View style={this.state.show == 'open'? styles.content : styles.contentClose}>
              <TouchableOpacity onPress={()=>this.close()}>
                <View style={styles.line}></View>
              </TouchableOpacity>

              <View style={{flex:1, height:350}}>
                <Tabs 
                tabBarActiveTextColor={'#1890ff'} 
                tabs={tabs}
                >
                  <View style={{flex:1,paddingBottom:isIphoneX()?10:0}}>
                    <GiftedChat
                      messages={this.state.messages}
                      onSend={messages => this.onSend(messages)}
                      // renderBubble={this._renderBubble}
                      renderTicks={this.renderTicks}
                      user={{
                        _id: 1,
                      }}
                    />
                  </View>
                  <View style={{flex:1, padding:10}}>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                      <Text style={styles.textA}>信號強度歷史紀錄</Text>
                      <Text onPress={()=>Actions.addstepthree({id:this.state.id})}>設定</Text>
                    </View>
                    <Text style={styles.textA}>追蹤目標：Jack</Text>
                    <Text style={styles.textA}>6月18號 下午2:00最後更新</Text>
                    <Tabs 
                      tabBarActiveTextColor={'#1890ff'} 
                      tabs={tabstwo}
                    >
                      <ScrollView style={{flex:1}}>
                        <Echarts option={option} height={150} />
                        <Echarts option={option2} height={100} />
                      </ScrollView>
                      <View style={{flex:1, height: 42}}>
                        <Echarts option={option2} height={200} />
                      </View>
                    </Tabs>
                  </View>
                </Tabs>
              </View>
            </View>
                     {/* <Text style={styles.sectionTitle} onPress={() => this.test()}>扫描可2用设备</Text> */}
            <MapView
              style={styles.mapstyle}
              ref={map => this._map = map}
              showsUserLocation={true}
              showsMyLocationButton={true}
              provider={PROVIDER_GOOGLE}
              zoomTapEnabled={false}
              // onPress={()=>this.close()}
              // region={this.state.initialPosition}
              initialRegion={this.state.initialPosition}
            >
            <Circle 
              center={{latitude:this.state.circlelatitude,longitude:this.state.circlelongitude}}
              radius={this.state.radius}
              fillColor={'rgba(200,300,200,0.8)'}
              zIndex={100}
            />
            {
              this.state.devicedata.map((item,i) => {
                return <Marker
                ref={e => this.state.markers[i] = e}
                
                onPress={()=>this.onItemChange(item,i)}
                pinColor={'#359596'}
                coordinate={{latitude:item.latitude,longitude:item.longitude}}
              >
                  <Callout>
                    <View >
                      <View style={styles.infoCardTop}>
                        <View style={{alignItems:'center'}}>
                          <Icon name="user" size={50} color="#174638" onPress={this.close}/>
                        </View>
                        <Text style={styles.textB}>追蹤目標： {item.name}</Text>
                        <Text style={styles.textB}>安全距離：正常{item.distance_threshold}公尺以內</Text>
                        <Text style={styles.textB}>跟蹤時間：2020/06/12/ 12:12</Text>
                        <Text style={styles.textB}>訊號狀態：強</Text>
                      </View>
                    </View>
                  </Callout>
                </Marker>
              })
            }


          </MapView>
          </View>
    </Drawer>
    </Provider>
  )};
};

const styles = StyleSheet.create({
  container:{
    flex:1,
    alignItems:'center'
  },
  content:{
    padding:10,
    position:"absolute",
    width:'100%',
    bottom:0,
    zIndex:100,
    backgroundColor:'#fff',
    alignItems:'center',
    borderTopLeftRadius:20,borderTopRightRadius:20
  },
  line:{
    width:50,
    height:6,
    borderRadius:3,
    backgroundColor:'#ccc',
    alignItems:'center'
  },
  infoCard: {
    width:160,
    height:200,
    backgroundColor:'#fff',
    zIndex:100,
    position:'absolute',
    top:70,
    right:30,
    borderRadius:5,
    padding:10
  },
  infoCardnone: {
    display:"none"
  },
  infoCardTop: {
    flex:1,
    textAlign:"right",
    padding:10,
  },
  textB:{
    marginBottom:5
  },
  img: {
    width:50,
    height:50,
    borderRadius:25,
  },
  contentClose: {
    height:0
    // display:'none'
  },

  mapstyle: {
    width:'100%',
    flex:1,
    
    marginBottom:isIphoneX()?20:0
  },
  sideclose: {
    position:'absolute',
    right:-13,
    top:26,
    zIndex:1001,
    width:26,
    height:26,
    borderRadius:13,
    backgroundColor:'#999',
    textAlign:'center',
    lineHeight:30,
  },
  sideshow: {
    position:'absolute',
    left:0,
    top:44,
    zIndex:1000,
    width:26,
    height:26,
    borderRadius:3,
    backgroundColor:'#000',
    textAlign:'center',
    lineHeight:30,
  },
  textA:{
    color:'#666',
    marginBottom:5
  }
});

