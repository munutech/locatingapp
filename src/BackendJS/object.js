import { getStatus, setStatus } from './status.js'
import SQLite from '../utils/SQLite';
var sqLite = new SQLite();
var db;
var object_list = [];


export function getObjectList()
{
    return object_list;
}
export function getdata() {
  if(!db){
      db = sqLite.open();
  }
  //查询
  sqLite.searchDevice()

}

export function ondelete() {
  sqLite.dropTable()
}


export function regObject(obj)         // success : return 0, fail :  none
{
    // var obj = {
    //     "sn" :  str_sn, //"22222222", // serial number
    //     "bt_id" : "bt" + str_sn, //"bt123456", // bluetooth id
    //     "name" :  "name_test", // alias name
    //     "mode" :  0, // 登山, …..etc
    //     "distance_threshold" : 1.1, // float
    //     "polling_frequency" : 1, //sec
    //     "signal_threshold" :  2.2, // float
    //     "latitude" : 0.0003,
    //     "longitude" : 0.0001
    // };
    // object_list.push(obj);


    sqLite.createTable();

    const deviceData = []
    deviceData.push(obj); 
    sqLite.insertDeviceData(deviceData);



  //  var latest_status = {
  //   "sn" :  str_sn, //"22222222", // serial number
  //   "distance" : 1.1, // float
  //   "polling_frequency" : 1, //sec
  //   "signal_threshold" :  2.2, // float
  //   "latitude" : 0.0003,
  //   "longitude" : 0.0001,
  //   "distance" : 1, 
  //   "distance_level" : 0, //0: offince, 1 : low, 2 : normal
  //   "distance_boundary" : "100", //distance in meters
  //   "obj_battery_level" : 0, // 0 : low, 1 : normal
  //   "obj_rssi" : 3,  // strength
  //   "obj_connect_level" : 0, //0 : offline , 1 : weak, 2 : middle, 3 : strong 
  //   "dev_rssi" : 1,
  //   "dev_connect_level" : 0, //0 : offline , 1 : weak, 2 : middle, 3 : strong 
  // };



    // setStatus(str_sn, latest_status);

    // return 0;
}
export function getObject(str_sn)         //  success : return JSON Object , fail :  none
{
    console.log("getObject : " + str_sn);


    for(var i = 0; i < object_list.length; i++)
    {
      if(object_list[i].sn == str_sn)
      {
        return object_list[i];
      }
    }

    return null;

}
export function setObject(str_sn, json_obj)  //
{
 

    console.log("setObject");
    for(var i = 0; i < object_list.length; i++)
    {
      if(object_list[i].sn == str_sn)
      {
        object_list[i] = json_obj;
        return json_obj;
      }
    }

    object_list.push(json_obj);
    return json_obj;

 
}

export function setObjectAttribute(str_sn, str_attr, void_value)
{
    console.log("setObjectAttribute");

    var myobj = getObject(str_sn);
    
    if(myobj == null)
    { console.log("No such sn : " + str_sn);
      return null; }

    
    for (var key in myobj) {
      //console.log(key);
      if (key == str_attr) {
      //if (key.match(str_attr)) {
        myobj[str_attr] = void_value;
        //console.log(key + " = " + myobj[str_attr]);
        return myobj;
      }
    }  
    

    return null;
}

export function getObjectAttribute(str_sn, str_attr)
{
    console.log("setObjectAttribute");

    var myobj = getObject(str_sn);
    
    if(myobj == null)
    { console.log("No such sn : " + str_sn);
      return null; }

    
    for (var key in myobj) {
      //console.log(key);
      if (key == str_attr) {
      //if (key.match(str_attr)) {
        return myobj[str_attr];
        //console.log(key + " = " + myobj[str_attr]);
  
      }
    }  
    

    return null;
}


export function smokeTest()
{
  console.log("add SN - 11111111");
  regObject('11111111');

  var mystatus = getStatus('11111111');
  console.log(mystatus);


  console.log("add SN - 22222222");
  regObject('22222222');

  console.log("get list");
  var mylist = getObjectList();
  console.log(mylist);






  console.log("get 22222222");
  var myobj = getObject("22222222");
  console.log(myobj);

  console.log("change name to \"second\" on 22222222");
  var myobj2 = setObjectAttribute("22222222", "name", "second");
  console.log(myobj2);



  console.log("update a whole object");
  var myobj3_1 = getObject("11111111");
  if(myobj3_1 != null)
  {
    var myobj3_2 = Object.assign({}, myobj3_1);
    myobj3_2["sn"] = "33333333"
    var myobj3_3 = setObject("33333333", myobj3_2); 
    var mylist = getObjectList();
    console.log(mylist);
  }






}
export function testHello()
{
  return "objHelloString";
}