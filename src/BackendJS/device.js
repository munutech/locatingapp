var device = { // if device is 
    "dev_id" : "12345678", //bluetooth id
    "dev_battery_level" : 0, // 0 : low, 1 : normal
    "dev_rssi" : 3,  // strength
    "dev_connect_level" : 0, //0 : offline , 1 : weak, 2 : middle, 3 : strong 
};

export function getDevice()
{
    return device;

}
