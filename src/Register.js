
import React,{Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Keyboard,
  TouchableWithoutFeedback
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Input,Button } from 'react-native-elements';
import {Actions} from 'react-native-router-flux'
import { Provider, Toast } from '@ant-design/react-native';

export default class Register extends Component{
    constructor(props){
        super(props)
        this.state={
            mobile:'',
            password:'',
            email:'',
            isPhoneError:false,
            isEmailError:false,
            // isError:false,
        }
    }

    to=()=>{
      if(this.state.password && this.state.mobile ){
        Actions.registerinfo({'mobile':this.state.mobile,'password':this.state.password,'email':this.state.email })
      }else{
        Toast.info('請輸入正確的手機號碼')
      }
    }

    lostBlur = () => {
      Keyboard.dismiss()
    }
    render() {
        return (
          <Provider>
          <TouchableWithoutFeedback onPress={()=>this.lostBlur()}>
            <View style={styles.container}>
              <Input
                placeholder='請輸入手機號碼'
                label={
                  <Text style={styles.lable}>請輸入手機號碼<Text style={{color:'red'}}>*</Text></Text>
                }
                errorMessage={this.state.isPhoneError? '請輸入正確手機號碼':''}
                errorStyle={{marginHorizontal:40,marginBottom:10}}
                onChangeText={(mobile)=>{
                  if(!(/^[1][3-8]\d{9}$|^([6|9])\d{7}$|^[0][9]\d{8}$|^[6]([8|6])\d{5}$/.test(mobile))){ 
                    this.setState({
                      mobile:mobile,
                      isPhoneError: true
                    })
                    return false; 
                  }else{
                    this.setState({
                      mobile:mobile,
                      isPhoneError:false
                    })
                  }
                }}
                rightIcon={
                    <Icon
                    name='mobile'
                    size={40}
                    color='#999'
                    />
                }
                inputContainerStyle={{borderBottomColor:'#ccc',marginHorizontal:40}}
                />
              <Input
                placeholder='請輸入Email'
                label={
                  <Text style={styles.lable}>請輸入Email<Text style={{color:'red'}}>*</Text></Text>
                }
                errorMessage={this.state.isEmailError? '請輸入正確Email':''}
                errorStyle={{marginHorizontal:40}}
                errorStyle={{marginHorizontal:40}}
                onChangeText={(email)=>{
                  if(!(/^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/.test(email))){ 
                    this.setState({
                      email:email,
                      isEmailError: true
                    })
                    return false; 
                  }else{
                    this.setState({
                      email:email,
                      isEmailError:false
                    })
                  }
                }}
                rightIcon={
                    <Icon
                    name='envelope'
                    size={20}
                    color='#999'
                    />
                }
                inputContainerStyle={{borderBottomColor:'#ccc',marginLeft:40,marginRight:40}}
                />
              <Input
                placeholder='請輸入密碼'
                label={
                  <Text style={styles.lable}>請輸入密碼<Text style={{color:'red'}}>*</Text></Text>
                }
                autoCompleteType='password'
                onChangeText={(password)=>this.setState({password})}
                value={this.setState.password}
                rightIcon={
                    <Icon
                    name='lock'
                    size={30}
                    color='#999'
                    />
                }
                inputContainerStyle={{borderBottomColor:'#ccc',marginLeft:40,marginRight:40}}
                />
                <Button
                 containerStyle={{padding:40,width:'100%',marginLeft:40,marginRight:40}} 
                 buttonStyle={{height:48,borderRadius:24,backgroundColor:'#3578E5'}} title='註冊' 
                 onPress={()=>this.to()}>
                </Button>
                {/* <View style={styles.bottom}>
                  <Text style={{lineHeight:18}}>使用太平煤氣會員，即表示您已閱讀、了解，並同意接受我們的「<Text style={styles.gfont}>服務條款</Text>」和「<Text style={styles.gfont}>隱私聲明</Text>」</Text>
                </View> */}

            </View>
            </TouchableWithoutFeedback>
            </Provider>
          );
    }
  }


const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
      paddingTop:110
    },
    lable: {
      color:'#3578E5',
      paddingHorizontal:40,
      marginTop:10
    },
    lableRight:{
      color:'red',
    },
    gfont:{
      color:'#3578E5',
    },
    bottom:{
      paddingHorizontal:40,
    }
});
