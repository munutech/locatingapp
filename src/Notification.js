import React, { Component } from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  FlatList
} from 'react-native';
import { List, SwipeAction } from '@ant-design/react-native';

export default class Notification extends Component {
  render() {
    const right = [
      {
        text: 'Delete',
        onPress: () => console.log('delete'),
        style: { backgroundColor: 'red', color: 'white' },
      },
    ];
    return (
     <ScrollView style={StyleSheet.container}>
      <SwipeAction
        autoClose
        style={{ backgroundColor: 'transparent' }}
        right={right}
        onOpen={() => console.log('open')}
        onClose={() => console.log('close')}
      >
        <View style={styles.flexContainer}>
          <View style={styles.cellfixedL}>
            <View style={styles.img}></View>
          </View>
          <View style={styles.cell}>
            {/* <Text style={styles.welcome}> */}
              <Text style={styles.title} numberOfLines={2} >Jacley有五则留言给您 请注意查收请注意查收请注意查收请注意查收请注意查收请注意查收请注意查收请注意查收请注意查收请注意查收请注意查收请注意查收请注意查收请注意查收请注意查收请注意查收请注意查收请注意查收请注意查收请注意查收</Text>
              <Text>2020/07/06 12:00</Text>
            {/* </Text> */}
          </View>
          <View style={styles.cellfixedR}>
            <View style={styles.point}></View>
          </View>
        </View>
      </SwipeAction>

     </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  container:{
    flex:1
  },
  item: {
    flexDirection:'row',
    padding:20,
    borderBottomWidth:1,
    borderBottomColor:'#ccc',
    alignItems:'center',
    justifyContent:'space-between'
  },
  img: {
    width:60,
    height:60,
    borderRadius:30,
    backgroundColor:'#ccc',
    marginRight:20,
  },
  point: {
    width:10,
    height:10,
    borderRadius:5,
    backgroundColor:'blue'
  },
  title:{
    flex:1,
    marginBottom:15,
    fontSize:16,
    marginRight:10,
    flexWrap:'wrap'
  },
  flexContainer: {
    // 容器需要添加direction才能变成让子元素flex
    flexDirection: 'row',
    alignItems:'center',
    padding:10,
    borderBottomWidth:1,
    borderBottomColor:'#ccc',
  },
  cell: {
      flex: 1,
      // height: 50,
      // backgroundColor: '#aaaaaa'
  },
  welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10
  },
  cellfixedL: {
      // height: 50,
      width: 80,
      textAlign:'center',
      // backgroundColor: '#fefefe'
  },
  cellfixedR: {
    // height: 50,
    width: 20,
    textAlign:'center',
  }
})
