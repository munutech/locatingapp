
import React,{Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
  AsyncStorage,
  Keyboard,
  TouchableWithoutFeedback
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Input,Button } from 'react-native-elements';
import {Actions} from 'react-native-router-flux'
import { LoginButton, AccessToken } from 'react-native-fbsdk';
import { Provider} from '@ant-design/react-native';
// import {Provider} from '@ant-design/react-native';
export default class Login extends Component{
    constructor(props){
        super(props)
        this.state={
            mobile:'',
            password:'',
        }
    }
    login=()=>{
      const params = {
        account:this.state.mobile,
        password:this.state.password
      }

      Actions.app()
      
    }

    toRegister=()=>{
      
      Actions.register()
    }

    toForgerPassword=()=>{
      Actions.forgetPassword()
    }

    lostBlur = () => {
      Keyboard.dismiss()
    }

    render() {
        return (
          <Provider>

          <TouchableWithoutFeedback onPress={()=>this.lostBlur()}>
            <View style={styles.container}>
              <Text style={styles.title}>
                LOCATINGAPP
              </Text>
              <Text style={{fontSize:13,color:'rgb(80,80,80)',marginBottom:25}}></Text>
              <Input
                placeholder='請輸入手機號碼'
                label='請輸入手機號碼'
                onChangeText={(mobile)=>this.setState({mobile})}
                value={this.setState.mobile}
                labelStyle={{color:'#3578E5',marginLeft:40,fontSize:12}}
                rightIcon={
                    <Icon
                    name='mobile'
                    size={30}
                    color='#999'
                    />
                }
                inputContainerStyle={{borderBottomColor:'#ccc',marginLeft:40,marginRight:40,marginBottom:10}}
                />
              <Input
                label='請輸入密码'
                placeholder='請輸入密碼'
                secureTextEntry={true}
                onChangeText={(password)=>this.setState({password})}
                value={this.setState.password}
                labelStyle={{color:'#3578E5',marginLeft:40,fontSize:12}}
                password={true}
                rightIcon={
                    <Icon
                    name='lock'
                    size={30}
                    color='#999'
                    />
                }
                inputContainerStyle={{borderBottomColor:'#ccc',marginLeft:40,marginRight:40}}
                />
                <Button
                 containerStyle={{padding:40,paddingBottom:20,width:'100%',marginLeft:40,marginRight:40}} 
                 buttonStyle={{height:48,borderRadius:24,backgroundColor:'#3578E5'}} title='登入' 
                 onPress={()=>this.login()}>
                </Button>

                {/* <View style={styles.bottom}>
                  <Text style={{lineHeight:18}}>使用太平煤氣會員，即表示您已閱讀、了解，並同意接受我們的「<Text style={styles.gfont}>服務條款</Text>」和「<Text style={styles.gfont}>隱私聲明</Text>」</Text>
                </View> */}
                <LoginButton
                style={{width:150,height:30,borderRadius:24,marginBottom:20}}
                onLoginFinished={
                    (error, result) => {
                    if (error) {
                        console.log("login has error: " + result.error);
                    } else if (result.isCancelled) {
                        console.log("login is cancelled.");
                    } else {
                        AccessToken.getCurrentAccessToken().then(
                        (data) => {
                            AsyncStorage.setItem('token',data.accessToken.toString())
                            Actions.app()
                            console.log(data.accessToken.toString())
                        }
                        )
                    }
                    }
                }
                onLogoutFinished={() => console.log("logout.")}/>
                <View style={{fontSize:16,flexDirection:'row',alignItems:'center'}}>
                  <TouchableHighlight  onPress={()=>{this.toRegister()}} underlayColor='#fff'>
                    <View>
                      <Text style={styles.gfont}>加入會員</Text>
                    </View>
                  </TouchableHighlight>
                  <Text style={{color:'#d8d8d8',marginLeft:30,marginRight:30}}>|</Text>
                  <TouchableHighlight  onPress={()=>{Actions.forgetPassword()}} underlayColor='#fff'>
                    <View>
                      <Text style={styles.gfont}>忘記密碼</Text>
                    </View>
                  </TouchableHighlight>
                </View>
                <View style={styles.bottom}>
                </View>
            </View>
            </TouchableWithoutFeedback>
            </Provider>

          );
    }
  }


const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    title:{
      fontSize:31,
      marginTop:105,
      marginBottom: 5,
      fontFamily:'PingFangTC-Medium'
    },
    gfont:{
      color:'#3578E5',
    },
    bottom:{
      padding:40,
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    }
});
