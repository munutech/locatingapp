import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import Echarts from 'native-echarts';
import { GiftedChat, Bubble } from 'react-native-gifted-chat'

export default class Example extends Component {
  constructor(props){
    super(props)
    this.state={
      messages:[{
        _id: 2,
        text: '微信小程序开发的基本流程',
        createdAt: new Date('2018-10-25T15:41:00+08:00'),
        received:true,
        user: {
          _id: 1,
          name: 'jackson影琪',
          avatar: 'https://pic.cnblogs.com/avatar/1040068/20181013100635.png',
        },
        //image: 'https://img2018.cnblogs.com/blog/1040068/201810/1040068-20181024162047704-1159291775.png',
      },]
    }
  }
  onSend = (messages) => {
    this.setState(previousState=>({
      messages: GiftedChat.append(previousState.messages, messages),
    }))
  }

  _renderBubble=(props)=> {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          left: { //对方的气泡
            backgroundColor: '#F5F7F7',
          },
          right:{ //我方的气泡
            backgroundColor:'#FFF5EE'
          }
        }}
      />
    );
  }

  renderTicks=currentMessage=>{
    console.log(currentMessage)
    const tickedUser =  currentMessage.user._id
    return(

    <View>
    {!!currentMessage.sent && !!currentMessage.received && tickedUser===this.props.user.userId && this.props.user.userId && (<Text style={{color: 'gold', paddingRight:10}}>✓✓</Text>)}
    </View>
      )
  }
  render() {
    return (
      <View style={{padding:20,flex:1}}
      >
      <GiftedChat
      messages={this.state.messages}
      onSend={messages => this.onSend(messages)}
      // renderBubble={this._renderBubble}
      renderTicks={this.renderTicks}
      user={{
        _id: 1,
      }}
    />
      </View>

    );
  }
}