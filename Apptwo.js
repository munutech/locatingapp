/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  DeviceEventEmitter
} from 'react-native';
import TabNavigator from 'react-native-tab-navigator';
import Homepage from './src/Homepage'
import Example from './src/Example'
import Notification from './src/Notification'
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux'
import Actionsheet from './src/Actionsheet';
import {isIphoneX, ifIphoneX} from './src/utils/isIphonex'
import Addstepone from './src/Addstepone';
export default class App extends Component{

  constructor(props){
    super(props)
    this.state={
      selectedTab: 'notification'
    }
  }

  componentDidMount(){

  }

  handleClick = (selectedTab) => {

    // this.setState({
    //   selectedTab:selectedTab
    // })
    // DeviceEventEmitter.emit('isPage',selectedTab)
   Actions.app()

    console.log(this.state.selectedTab)
    
  }


  render() {

  return (
    <TabNavigator tabBarStyle={styles.tabBarStyle}>
      <TabNavigator.Item
        selected={this.state.selectedTab === 'home'}
        title="追踪状态"
        renderIcon={()=><Icon name="map-marker" size={25} color="#ccc" />}
        renderSelectedIcon={()=><Icon name="map-marker" size={25} color="#0079ff" />}
        onPress={() =>  Actions.app()}
        >
        <Homepage />
      </TabNavigator.Item>
      <TabNavigator.Item
        selected={this.state.selectedTab === 'notification'}
        title="通知"
        badgeText="10"
        renderIcon={()=><Icon name="bell" size={25} color="#ccc" />}
        renderSelectedIcon={()=><Icon name="bell" size={25} color="#0079ff" />}
        onPress={() => Actions.apptwo()}>
          <Notification />
      </TabNavigator.Item>
      <TabNavigator.Item
        selected={this.state.selectedTab === 'add'}
        title="新增追踪"
        renderIcon={()=><Icon name="plus-square-o" size={25} color="#ccc" />}
        renderSelectedIcon={()=><Icon name="plus-square-o" size={25} color="#0079ff" />}
        onPress={() => Actions.addstepone()}>
      </TabNavigator.Item>
      <TabNavigator.Item
        selected={this.state.selectedTab === 'mine'}
        title="我的"
        renderIcon={()=><Icon name="user-o" size={25} color="#ccc" />}
        renderSelectedIcon={()=><Icon name="user-o" size={25} color="#0079ff" />}
        onPress={() => Actions.appthree()}>
        <Example />
      </TabNavigator.Item>
    </TabNavigator>
           
  )};
};

const styles = StyleSheet.create({
  tabBarStyle:{
    height:isIphoneX()? 74:49,
    paddingBottom:isIphoneX()?24:0
    
  }
});

