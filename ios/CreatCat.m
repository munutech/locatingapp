//
//  CreatCat.m
//  demo
//
//  Created by xieyusheng on 2017/11/10.
//  Copyright © 2017年 Facebook. All rights reserved.
//
 
#import <Foundation/Foundation.h>
#import "CreatCat.h"
 
@implementation CreatCat
 
RCT_EXPORT_MODULE();
 
RCT_EXPORT_METHOD(cratCat:(NSString *)name sex:(NSString *)sex age:(int)age)
{
  NSLog(@"我创建了一只名叫%@的猫，性别%@， 今年%d岁", name, sex, age);
}
 
//对外提供调用方法
RCT_EXPORT_METHOD(addEvent:(NSString *)name location:(NSString *)location){
  NSLog(@"Pretending to create an event %@ at %@", name, location);
}
 
//方法3
RCT_EXPORT_METHOD(whoName:(NSString *)name age:(int)age location:(NSString *)location){
  NSLog(@"可以的%@,年来%d,还有%@",name,age,location);
}
 
@end
