
import React,{Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  DeviceEventEmitter,
  AsyncStorage
} from 'react-native';
import {Scene,ActionConst, Router, Actions,Stack} from 'react-native-router-flux'
import App from './App'
import Apptwo from './Apptwo'
import Appthree from './Appthree'
import Addstepone from './src/Addstepone'
import Addsteptwo from './src/Addsteptwo'
import Addstepthree from './src/Addstepthree'
import Notification from './src/Notification'
import SQLiteDemo from './src/SQLiteDemo'
import Login from './src/Login'
import Register from './src/Register'
import Mine from './src/Mine'
import CameraQr from './src/Camera'
import Appplx from './Appplx'

import Icon from 'react-native-vector-icons/FontAwesome';
import * as bkjs from './src/BackendJS/object.js'


export default class Main extends Component{
  constructor(props){
    super(props)
    this.state={
      hasToken:'',
      selectTab:'home',
      tabName:'App',
      isHideNavBar:true,
      objectData:[]
    }
  }
  componentDidMount(){  
    this.hasToken()
    //监听新增
    // this.listenerValue = DeviceEventEmitter.addListener('getValue',(params)=>{
    //   this.setState({
    //     objectData:params
    //   })
    // })
    //监听首页
    // this.listener = DeviceEventEmitter.addListener('isPage',(params)=>{
    //     this.setState({
    //       selectTab:params
    //     })
    //     switch(params){
    //       case 'home':
    //         this.setState({
    //           tabName:'首頁',
    //           isHideNavBar:true
    //         });
    //         break;
    //       case 'notification':
    //         this.setState({
    //           tabName:'通知',
    //           isHideNavBar:false
    //         });
    //         break;
    //       case 'mine':
    //         this.setState({
    //           tabName:'我的',
    //           isHideNavBar:false
    //         });
    //         break;          
    //     }
    // });
  }
 
  componentWillUnmount(){
    // this.listenerValue.remove();
    // this.listener.remove();
  }

  UNSAFE_componentWillMount(){
    // this.listenerValue = DeviceEventEmitter.addListener('getValue',(params)=>{
    //   this.setState({
    //     objectData:params
    //   })
    // })

  }

  hasToken = async() => {
    this.setState({
      hasToken:await AsyncStorage.getItem('token')?true:false
    })
    console.log(await AsyncStorage.getItem('token'))

  }


  editstepone = () => {
      DeviceEventEmitter.emit('isEditNotice',1)
  }

  editstepthree = () => {

    console.log(this.state.objectData)
    // DeviceEventEmitter.emit('getValue',this.state)

  }

  confirm = () => {
    bkjs.regObject(15072498399)
  }
  



  render() {
        return (  
            <Router>
                <Scene key="root">
                    <Scene key="login" component={Login} title="登陸" initial={!this.state.hasToken}/>
                    <Scene key="appthree" component={Mine} title="我的" />
                    <Scene key="register" component={Register} title="註冊" />
                    <Scene key="app" component={App} title={'首頁'} hideNavBar={this.state.isHideNavBar} type={ActionConst.RESET} initial={this.state.hasToken}/>
                    <Scene key="apptwo" component={Apptwo} title={'通知'} hideNavBar={false} type={ActionConst.RESET}/>
                    {/* <Scene key="appthree" component={Appthree} title={'我的'} hideNavBar={false} type={ActionConst.RESET}/> */}
                    {/* <Scene key="appthree" component={Appplx} title={'我的'}  /> */}
                    <Scene key="addstepone" component={Addstepone} hideNavBar={false} title="新增追蹤物件" 
                      type={ActionConst.RESET}
                      renderLeftButton={
                        <View style={styles.leftButtonStyle}>
                          <Icon name='close' size={25} color='#000' onPress={()=>Actions.app()}/>
                        </View>
                      }
                       renderRightButton={
                        <View style={styles.leftButtonStyle}>
                          <Text onPress={()=>this.editstepone()}>
                            下一步
                          </Text>
                        </View>
                      }                    
                    />
                    <Scene key="addsteptwo" component={Addsteptwo} title="新增追蹤物件" 
                      type={ActionConst.RESET}
                      renderLeftButton={
                        <View style={styles.leftButtonStyle}>
                          <Icon name='close' size={25} color='#000' onPress={()=>Actions.app()}/>
                        </View>
                      }
                    />
                    <Scene key="addstepthree" component={Addstepthree} title="新增追蹤物件" 
                      type={ActionConst.RESET}
                      renderLeftButton={
                        <View style={styles.leftButtonStyle}>
                          <Icon name='close' size={25} color='#000' onPress={()=>Actions.app()}/>
                        </View>
                      }
                      renderRightButton={
                        <View style={styles.leftButtonStyle}>
                          <Text onPress={()=>this.editstepthree()}>
                            確認
                          </Text>
                        </View>
                      }
                    />
                    <Scene key="notification" component={Notification} title="通知" />
                    <Scene key="scan" component={CameraQr} title="扫一扫" />


                </Scene>
            </Router>

          );
  }
}
  

  const styles = StyleSheet.create({
    leftButtonStyle:{
      paddingHorizontal:10
    }

  })